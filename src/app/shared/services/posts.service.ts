import {Injectable} from '@angular/core';
import {BaseApi} from './base-api';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {isNullOrUndefined} from 'util';

@Injectable({
  providedIn: 'root'
})
export class PostsService extends BaseApi {

  options: HttpHeaders;

  constructor(public http: HttpClient) {
    super(http);
    this.options = new HttpHeaders();
    this.options = this.options.set('Content-Type', 'application/json');
  }

  getUserPosts(id: number) {
    return this.get('posts?userId=' + id , this.options).toPromise();
  }
}
