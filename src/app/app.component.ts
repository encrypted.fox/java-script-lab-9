import {Component, OnInit} from '@angular/core';
import {UsersService} from './shared/services/users.service';
import {PostsService} from './shared/services/posts.service';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  users: [{name, id, username, postsCount: any}];
	posts;

  constructor(private usersServise: UsersService,
              private postsService: PostsService) {
  }

  async ngOnInit() {

    try {

			let users = this.usersServise.getUsers();
      this.users = (isNullOrUndefined(await users)) ? [] : await users;
      
		} catch (err) {
			console.log(err);
    }
    
		try {

			for (let i = 0; i < this.users.length; i++){

				let posts = this.postsService.getUserPosts(this.users[i].id);
				this.posts = (isNullOrUndefined(await posts)) ? [] : await posts;
        this.users[i].postsCount = this.posts.length;
      
      }
      
		} catch(err) {
			console.log(err);
		}
  }
}
